﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileFile : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        //when the bullet collides with something
        if (!other.isTrigger) //ignores trigger colliders
        {
            if (other.gameObject.CompareTag("Enemy"))
            {
                //if bullet hits enemy
                EnemyHealth eHealth = other.gameObject.GetComponent<EnemyHealth>(); //returns reference of enemy health

                if (eHealth != null)
                    eHealth.TakeDamage(1); //cause one damage to enemy health
            }
            Destroy(gameObject);//destroys projectile
        }
    }
}
